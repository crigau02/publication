Publications
===================

Publication sources, algorithm, code, result, conference poster, scientific papers in computer vision and more especially document analysis domain.

See up-to-date publication list [here](https://dblp.uni-trier.de/pers/hd/r/Rigaud:Christophe.html).

