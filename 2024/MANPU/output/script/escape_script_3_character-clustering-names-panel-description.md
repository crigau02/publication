# PAGE 1 - 3 PANELS
## PANEL 1
?
### CAPTION
   ESCAPE with ME
   TRUE LOVE STORY
   DON'T KNOW QUITE WHAT I EXPECT ED WHEN I CAME HOME FROM COL-LEGE LAST YEAR, BUT I WAS SURE THINGS WERE GOING TO BE DIFFERENT FROM WHAT THEY HAD BEEN ALL MY LIFE. I WAS YOUNG AND I WANTED TO GO PLACES AND DO THINGS WANTED TO BE SOMEBODY! BUT MY PARENTS HAD OTHER IDEAS THEY WANTED ME TO STAY IN CARLE-TON AND SETTLE DOWN THERE. I WANTED TO BREAK AWAY BUT I DIDN'T KNOW HOW UNTIL THE DAY I MET BILL PATTERSON, AND FOUND WHAT WANTED! BUT HE DIDN'T WANT ME…..
### DIALOGUE
   Cynthia: BILL! PLEASE DON'T…
   Bill: I THOUGHT YOU WERE DIFFERENT, CYNTHY! BUT YOU'RE JUST LIKE THE REST OF THEM! RICH, SPOILED AND USELESS! I'M SICK OF LOOKING AT YOU!
   Curt: SEE HERE, PATTERSON! WON'T STAND FOR THAT KIND OF TALK FROM A SERVANT!   
## PANEL 2
?
### CAPTION
   I KNEW MY PARENTS MEANT WELL, BUT THEY WERE ONLY STIFLING ME!
### DIALOGUE
   Dad: WHY DO YOU KEEP ARGUING, CYNTHIA? WE DON'T WANT YOU TO WASTE YOUR LIFE GRUBBING FOR AN EXISTENCE! WE HAVE MONEY, AND WE WANT YOU TO USE IT!
   Cynthia: BUT CAN'T YOU SEE? I'M NOT HAPPY! JUST GIVE ME ENOUGH MONEY TO GET STARTED IN NEW YORK… THAT'S ALL I WANT!
## PANEL 3
?
### CAPTION
   THEY JUST WOULDN'T LISTEN! IT WAS LIKE TALKING TO A BLANK WALL!
### DIALOGUE
   Gloria: YOU WOULDN'T LIKE IT, CYNTHIA! YOU'RE USED TO THIS LIFE! YOU COULDN'T ADJUST YOUR-SELF TO AN ORDINARY JOB! WE KNOW BEST, DEAR!
   Cynthia: IT'S NO USE TALKING TO YOU! YOU'VE NEVER UNDERSTOOD ME, AND YOU NEVER WILL!

# PAGE 2 - 7 PANELS
## PANEL 1
In the comic book panel, we see two young adults named Curt and Cynthia. Curt is in the foreground, holding a tennis racket, and Cynthia is in the background, looking surprised. They are standing next to a parked car. Curt is saying, "CURT! OH... I FORGOT WE WERE GOING TO PLAY TENNIS!" Cynthia is responding, "WELL... OKAY, HONEY! I'D JUST AS SOON GO FOR A DRIVE, ANYWAY!"
### DIALOGUE
   Cynthia: CURT! OH… I FORGOT WE WERE GOING TO PLAY TENNIS!
   Curt: WELL… OKAY, HONEY! I'D JUST AS SOON GO FOR A DRIVE, ANYWAY!
## PANEL 2
Curt, a young man with blond hair and a yellow jacket, is talking to Cynthia, a young woman with black hair and a red dress, in a convertible. Curt has his hand on the steering wheel, and Cynthia is looking out the passenger window. Curt: You’ve seemed so unhappy lately, Cynthy! I wish there was something I could do! I wish you’d let me try and make you happy! Cynthia: ...
### CAPTION
   I WAS A BAD MOOD, AND CURT SENSED IT IMMEDIATELY…
### DIALOGUE
   Curt: YOU'VE SEEMED SO UNHAPPY LATELY, CYNTHY! I WISH THERE WAS SOMETHING I COULD DO! I WISH YOU'D LET ME TRY AND MAKE YOU HAPPY!
   Cynthia:
## PANEL 3
In the comic book panel, a caption at the top reads, "I knew what was coming, but I didn't want to give him an answer.. Not then. I tried to change the mood!". Below the caption, we see a scene with two characters seated in a convertible car. The woman, Cynthia, has dark hair and wears a blue top. She is turned towards the man, Curt, who has blond hair. Cynthia says, "Goodness, Curt, you worry about me too much! C'mon, let's go for a swim!". Curt's response is not shown in the panel; his speech bubble is blank.
### CAPTION
   I KNEW WHAT WAS COMING, BUT I DIDN'T WANT TO GIVE HIM AN ANSWER.. NOT THEN. I TRIED TO CHANGE THE MOOD!
### DIALOGUE
   Cynthia: GOODNESS, CURT, YOU WORRY ABOUT ME TOO MUCH! C'MON, LET'S GO FOR A SWIM!
   Curt:
## PANEL 4
Cynthia: I liked Curt but I didn’t love him, and I knew that marrying him would only be a surrender to my parents. I had to lick this problem myself ... not give in to it! Later that evening, we dropped in at Gloria's; everybody gathered at Gloria's.
### CAPTION
   I LIKED CURT BUT 1 DIDN'T LOVE HIM, AND I KNEW THAT MARRYING HIM WOULD ONLY BE A SURRENDER TO MY PARENTS. I HAD TO LICK THIS PROBLEM MYSELF … NOT GIVE IN TO IT! LATER THAT EVENING, WE DROPPED IN AT GLORIA'S EVERYBODY GATHERED AT GLORIA'S --
### DIALOGUE
   Cynthia:
## PANEL 5
In this comic book panel, we're looking into a scene at a social gathering. Cynthia, a woman with short, dark hair dressed in a yellow dress, cheerfully enters the room and greets the group with enthusiasm, saying, "HI, KIDS! WHAT'S UP?". Bill, a man with neatly combed hair in a suit, responds with a dry wit, "SPARKLING WIT… BRILLIANT CONVERSATION… PENETRATING THOUGHT… WHAT DID YOU EXPECT?". Another voice, though the speaker isn't visually identified in the panel, adds a humorous touch of reality to the conversation by commenting, "HE MEANS IT'S THE USUAL DULL EVENING!". The atmosphere of the scene is one of light-hearted banter, with the characters engaging in playful sarcasm about the nature of their social interaction. The illustration shows several other guests in the background, suggesting a common social scene. Curt, a man with blonde hair, is present but remains silent in this particular exchange.
### DIALOGUE
   Cynthia: HI, KIDS! WHAT'S UP?
   Bill: SPARKLING WIT… BRILLIANT CONVERSATION… PENETRATING THOUGHT… WHAT DID YOU EXPECT?
   Curt:
   ?: HE MEANS IT'S THE USUAL DULL EVENING!
## PANEL 6
Bill, a man in a gray suit, says "MY OLD MAN SAYS I'M GOING TO HAVE TO TAKE THAT ADVERTISING JOB IN NEW YORK!". Curt, a man in a green suit and tie, replies "YEAH, ME, TOO! HE'LL TAKE ME ON AT THE BANK, BUT I THINK I'D RATHER WORK IN MY UNCLE'S PUBLISHING HOUSE!". Cynthia, a woman with short dark hair, interjects "DID IT EVER OCCUR TO YOU THAT YOU'RE LUCKY TO GET JOBS?". An unnamed woman with long dark hair listens to the conversation. The caption at the top reads "THE CONVERSATION WANDERED ON AIMLESSLY…..".
### CAPTION
   THE CONVERSATION WANDERED ON AIMLESSLY…..
### DIALOGUE
   Bill: MY OLD MAN SAYS I'M GOING TO HAVE TO TAKE THAT ADVERTISING JOB IN NEW YORK!
   Curt: YEAH, ME, TOO! HE'LL TAKE ME ON AT THE BANK, BUT I THINK I'D RATHER WORK IN MY UNCLE'S PUBLISHING HOUSE!
   Cynthia: DID IT EVER OCCUR TO YOU THAT YOU'RE LUCKY TO GET JOBS?
   ?:
## PANEL 7
In the image, we see a comic panel where the character Bill is expressing his disinterest in working. His words are: "LUCKY? MY OLD MAN'S GOT ENOUGH DOUGH TO SUPPORT HALF THIS TOWN! WHY SHOULD I WANT TO WORK?". The character Cynthia is present, and she says, "IF YOU DON'T KNOW, I'M NOT GOING TO TELL YOU!". The question mark character is asking, "ARE YOU SPEAKING TO ME?".
### DIALOGUE
   Bill: LUCKY? MY OLD MAN'S GOT ENOUGH DOUGH TO SUPPORT HALF THIS TOWN! WHY SHOULD I WANT TO WORK?
   Cynthia: IF YOU DON'T KNOW, I'M NOT GOING TO TELL YOU!
   ?: ARE YOU SPEAKING TO ME?

# PAGE 3 - 7 PANELS
## PANEL 1
### DIALOGUE
   Cynthia: OH! HELLO.. WHO ARE YOU?
   Bill: I WORK FOR GLORIA'S FATHER… TAKING CARE OF THE GARDEN AND THE CAR AND THAT SORT OF THING! WHO ARE YOU?
## PANEL 2
### DIALOGUE
   Cynthia: I'M CYNTHY ALLEN! I MUST SAY… YOU DON'T LOOK MUCH LIKE A GARDENER!
   Bill: I'M NOT, OR AT LEAST, I WASN'T! DON'T TELL THE BOSS, BUT I NEVER DID ANYTHING BUT CUT LAWNS BEFORE I TOOK THIS JOB!
## PANEL 3
### DIALOGUE
   Cynthia: WHY DID YOU TAKE THE JOB, THEN? CERTAINLY NOT FOR THE MONEY!
   Bill: MAYBE I LIKE THE OUTDOOR LIFE! PROBABLY MY PIONEER BLOOD!
   Curt: CYNTHY! OH, THERE YOU ARE!
## PANEL 4
### DIALOGUE
   Bill: THREE'S A CROWD… S0 LONG!
   Curt: WHO WAS THAT?
   Cynthia: I DON'T KNOW! BUT I WISH I DID…
## PANEL 5
### CAPTION
   THREE DAYS LATER I RAN INTO HIM AT THE GAS STATION.
### DIALOGUE
   Cynthia: HI, THERE! WHAT ARE YOU UP TO?
   Bill: CAR WOULDN'T START! ANY HALF-WIT WOULD HAVE FIGURED OUT IT WAS BAD SPARK PLUGS, BUT IF A CAR'S GOT GAS IN IT, I CAN'T FIGURE OUT WHY IT WON'T RUN!
## PANEL 6
### DIALOGUE
   Cynthia: YOU SEEM AWFULLY OUT OF PLACE ON THIS JOB, OR HAVE I SAID THAT BEFORE?
   Bill: LOTS OF PEOPLE ARE OUT OF PLACE… EVEN YOU, MAYBE! I GET THREE SQUARE MEALS A DAY AND A PLACE TO SLEEP! I'M NOT KICKING!
## PANEL 7
### DIALOGUE
   Bill: TELL YOU WHAT! SEEING AS YOU'VE GOT SO MANY QUESTIONS TO ASK ME, WHY DON'T WE CONTINUE IT TONIGHT? SUPPOSE I PICK YOU UP ABOUT EIGHT?
   Cynthia: WHY… I… SURE, WHY NOT? OKAY… SEE YOU THEN!

# PAGE 4 - 7 PANELS
## PANEL 1
### CAPTION
   I HATED TO SEEM LIKE AN OLD SNOOP, BUT HE WAS A PUZZLE TO ME, AND I COULDN'T REST UNTIL I'D FIGURED IT OUT!
### DIALOGUE
   Cynthia: YOU KNOW… I JUST REALIZED! I DON'T EVEN KNOW YOUR NAME! I REALLY DON'T KNOW ANYTHING ABOUT YOU!
   Bill: OKAY, CYNTHY… YOU WIN! THERE'S NO REAL REASON FOR ME TO BE SO SECRETIVE, ANYWAY! MY NAME'S BILL PATTERSON AND I COME FROM INDIANA!
## PANEL 2
### DIALOGUE
   Bill: I CAME TO NEW YORK LAST YEAR WITH FIVE HUNDRED BUCKS AND A HATFUL OF HOPES! I WAS GONNA GET A JOB IN ONE OF THOSE BIG SKYSCRAPERS, AND KNOCK THE TOWN ON ITS EAR! SEVEN MONTHS LATER, MY DOUGH RAN OUT…
## PANEL 3
### DIALOGUE
   Bill: I TOOK A JOB AS A BUS BOY! I WAS LIVING IN A FIFTH FLOOR WALKUP, AND I ONLY SAW THE SUN ON WEEK ENDS! THEN I SAW THIS JOB ADVER-TISED IN THE SUNDAY PAPER, AND HERE I AM!
   Cynthia: THEN YOU JUST GAVE UP! THINGS WERE TOO TOUGH, SO YOU QUIT ON ALL YOUR DREAMS! I CAN SEE WHY YOU WOULDN'T TELL ME ANYTHING… YOU'RE ASHAMED OF YOURSELF!
## PANEL 4
### DIALOGUE
   Bill: YOU'RE A GREAT ONE TO TALK! WHEN HAVE YOU EVER FACED ANYTHING LIKE THAT? YOU'VE ALWAYS HAD SECURITY… YOU DON'T KNOW WHAT IT'S LIKE TO BE WITHOUT IT!
   Cynthia: I'M SORRY; BILL… I DIDN'T HAVE ANY RIGHT TO SOUND OFF THAT WAY! LET'S… LET'S TALK ABOUT SOMETHING ELSE!
## PANEL 5
### CAPTION
   BILL COULDN'T KNOW THAT I HATED MY SECURITY AS MUCH AS HE RE-SENTED IT, AND I COULDN'T TELL HIM WITHCUT.RE-VEALING MY OWN WEAKNESS… WEAK-BECAUSE I NESS NEVER HAD THE COURAGE TO BREAK AWAY! BILL START-ED DROPPING OVER IN THE AFTER-NOONS, AND I SLOW-LY BEGAN TO FOR-GET MY DREAMS OF CONQUERING THE CITY..
## PANEL 6
### CAPTION
   WE BEGAN SPENDING TIME TOGETHER EVERY DAY…
### DIALOGUE
   Bill: YOU KEEP ASKING QUESTIONS, CYNTHY… WHY ARE YOU SO INTERESTED IN ME?
   Cynthia: I DON'T KNOW, BILL…
## PANEL 7
### CAPTION
   SUDDENLY HIS LIPS WERE PRESSING ON MINE! I KNEW THIS WAS DESTINED TO HAPPEN IT HAD BEEN INEVITABLE EVER SINCE THAT FIRST NIGHT WE MET! AND I KNEW THAT THIS WAS DIFFERENT… DIF-FERENT FROM CURT … DIFFERENT FROM ANYTHING I HAD EVER KNOWN BEFORE!

# PAGE 5 - 7 PANELS
## PANEL 1
In this panel, we see a man and a woman in a relaxed setting. The man, who is named Bill, is standing and wearing a white tank top and blue shorts, holding a blue inflatable device in his hand. He is addressing the woman, who is named Cynthia, sitting comfortably in a red lounge chair, wearing a red bikini with a red towel draped over her shoulders. Bill is expressing his joy for having voted himself a couple of hours off, and he's curiously inquiring about the leisurely activities of the idle rich, specifically asking about Cynthia's afternoon plans. Cynthia, appearing to be fine, responds by urging Bill to put on his suit and join her for a swim, indicating that they are both in a swimming pool setting, and that the weather and environment are perfect for a swim. The relaxed atmosphere and the casual attire of the characters suggest a warm, sunny day, ideal for a swim and leisure time. The presence of the inflatable device in...
### CAPTION
   WHEN HE HELD ME IN HIS ARMS, I NEVER WANTED TO LEAVE HIM… NEVER WANTED TO GO BACK TO THAT HOUSE. BUT I HAD TO --THERE WERE RULES TO BE FOLLOWED. THE NEXT AFTERNOON….
### DIALOGUE
   Bill: HI! I JUST VOTED MYSELF A COUPLE OF HOURS OFF! HOW'S THE IDLE RICH THIS AFTERNOON?
   Cynthia: BILL! I'M FINE… PUT YOUR SUIT ON, AND WE'LL HAVE A SWIM!
## PANEL 2
Bill and Cynthia are lying on the grass next to a swimming pool. Bill is shirtless, and Cynthia is wearing a red swimsuit. They are talking to each other. Bill: It's meant a lot to me, Cynthy... meeting you! You've made me feel... different somehow, about a lot of things! Cynthia: I'm glad, Bill! You've meant a lot to me, too!
### DIALOGUE
   Bill: IT'S MEANT A LOT TO ME, CYNTHY… MEETING YOU! YOU'VE MADE ME FEEL… DIFFERENT SOMEHOW, ABOUT A LOT OF THINGS!
   Cynthia: I'M GLAD BILL! YOU'VE MEANT A LOT TO ME, TOO!
## PANEL 3
Once upon a time, there was a young couple named Bill and Cynthia. They were deeply in love, and their affection for each other was palpable. One day, as they stood close together, Bill's lips slowly came closer to Cynthia's. She wanted him to kiss her, but she also enjoyed the tingle and thrill of waiting for the moment. She knew that it wasn't just the kiss she was yearning for, but Bill himself. Bill, unable to hold back his feelings any longer, broke the silence. "I can't kid myself any more, baby! You mean everything to me... everything!" he confessed, his heart heavy with emotion. Overwhelmed by his words, Cynthia responded, her voice filled with a mix of surprise and joy, "Oh, Bill... Bill...". In that moment, the air was charged with anticipation and love. They didn't need words to express their feelings; their eyes spoke volumes. And as their lips finally met in a tender kiss, they knew that their love was meant to last forever.
### CAPTION
   HIS LIPS CAME SLOWLY CLOSER. I WANTED HIM TO KISS ME, BUT I ENJOYED THE TINGLING THRILL OF WAITING… I KNEW IT WASN'T JUST THE KISS IT WAS BILL!!
### DIALOGUE
   Bill: I CAN'T KID MYSELF ANY MORE, BABY! YOU MEAN EVERYTHING TO ME… VERYTHING!
   Cynthia: OH, BILL… BILL..
## PANEL 4
In the image, you see a comic book panel featuring two characters in a close embrace, sharing a passionate kiss. The character names are Bill and Cynthia, as indicated by the labels in the image. Bill and Cynthia are in the midst of a romantic moment, their lips pressed tightly together, indicating their affection. Bill's arms are wrapped around Cynthia, pulling her close, while Cynthia's arms are around Bill's neck. The intensity of their kiss suggests a deep emotional connection between the two characters. The background is a muted blue, giving the scene a somewhat dreamy quality, adding to the romantic atmosphere of the moment. The panel does not contain any text, so there is no dialogue to describe. The focus is purely on the emotion of the characters and their actions.
## PANEL 5
Certainly, here is my storytelling description of the comic panel: The scene opens with a biting laugh suddenly breaking the mood. We see Gloria, a blonde woman in a teal dress, remarking "Well! Our gardener seems to be moving up in the world!" Next to her stands Curt, a man in a brown suit, who responds with sarcasm "That seems obvious!". Meanwhile, in the foreground, we have Cynthia, caught off-guard while wearing only a towel, exclaiming "Curt! I didn't know you were coming...". Bill, shirtless and rugged-looking, stands beside Cynthia, suggesting he is the "gardener" Gloria referred to in her mocking comment about moving up in the world through his relationship with Cynthia. The background shows a suburban house and landscaping, setting the scene as a residential area where social perceptions and status are clearly at play in this awkward encounter.
### CAPTION
   SUDDENLY, WITHOUT A WORD OF WARNING, THE MOOD WAS BROKEN BY A HARSH, BITING LAUGH!
### DIALOGUE
   Gloria: WELL! OUR GARDENER SEEMS TO BE MOVING UP IN THE WORLD!
   Curt: THAT SEEMS OBVIOUS!
   Cynthia: CURT! I DIDN'T KNOW YOU WERE COMING…
## PANEL 6
In the comic panel, we see four characters engaged in a conversation. The first character is a man named Bill, who is speaking in a defensive tone, saying "YOU DON'T NEED TO BE SO NASTY ABOUT IT! OR CAN'T YOU HELP BEING NASTY?". The second character is a woman named Gloria, who appears to be confronting Bill. She says, "I SUPPOSE YOU REALIZE YOU'VE JUST TALKED YOURSELF OUT OF A JOB!". The third character is another woman named Cynthia, who is pleading with Bill, saying "WAIT… PLEASE! PLEASE, BILL… DON'T!". The fourth character is a man named Curt, who is standing behind Gloria and Cynthia, observing the exchange. The caption below the panel reads: "STILL WARM FROM BILL'S EMBRACE, I WAS CONFUSED AND UNABLE TO THINK. I WANTED TO CALM THEM, DOWN BEFORE SOMETHING TERRIBLE HAPPENED!"
### CAPTION
   STILL WARM FROM BILL'S EMBRACE, I WAS CONFUSED AND UNABLE TO THINK. I WANTED TO CALM THEM, DOWN BEFORE SOMETHING TERRIBLE HAPPENED!
### DIALOGUE
   Bill: YOU DON'T NEED TO BE SO NASTY ABOUT IT! OR CAN'T YOU HELP BEING NASTY?
   Gloria: I SUPPOSE YOU REALIZE YOU'VE JUST TALKED YOURSELF OUT OF A JOB!
   Cynthia: WAIT… PLEASE! PLEASE, BILL… DON'T!
## PANEL 7
In this comic book panel, we see three characters, each with a speech bubble. On the left, a character named Cynthia is speaking. She is wearing a red dress and has her hair styled in a neat bob. She is looking at another character, named Gloria, who is in the center of the panel. Gloria is wearing a green dress and has blonde hair. She is looking at a third character, named Curt, who is on the right. Curt is wearing a gray suit and has a cigarette in his mouth. He is looking at Gloria with a serious expression. The text in the panel is as follows: Cynthia: "DON'T BE ANGRY, GLORIA…". Gloria: "COME, CURT! IT'S NOT GOOD TASTE TO ARGUE WITH THE HIRED HELP!". Bill: "HOLD IT, YOU TWO! AS LONG AS I'M BEING CANNED, I MIGHT AS WELL GET A FEW THINGS OFF MY CHEST! YOU'RE ALL A LOT OF PHONIES, AND I WAS GETTING SICK OF YOU, ANYWAY!".
Curt:
### CAPTION
   I SHOULD HAVE SPOKEN UP AGAINST GLORIA AT ONCE, BUT I WAS SO AFRAID BILL WOULD LEAVE AND I WANTED TO CALM HER DOWN. I DIDN'T EXPECT BILL'S REACTION!
### DIALOGUE
   Cynthia: DON'T BE ANGRY, GLORIA…
   Gloria: COME, CURT! IT'S NOT GOOD TASTE TO ARGUE WITH THE HIRED HELP!
   Bill: HOLD IT, YOU TWO! AS LONG AS I'M BEING CANNED, I MIGHT AS WELL GET A FEW THINGS OFF MY CHEST! YOU'RE ALL A LOT OF PHONIES, AND WAS GETTING SICK OF YOU, ANYWAY!
   Curt:

# PAGE 6 - 7 PANELS
## PANEL 1
### DIALOGUE
   Bill: YOU'VE ALWAYS HAD EVERYTHING YOU WANTED, AND YOU NEVER ONCE APPRECIATED IT! ALL YOU GUYS TALKING ABOUT WHETHER YOU'LL TAKE THIS JOB OR THAT JOB… NOT REALIZING THERE ARE HUNDREDS OF GUYS WHO'D GIVE THEIR RIGHT ARM FOR ANY JOB!
### DIALOGUE
   Curt: YOU'VE SAID ENOUGH PATTERSON! GET OUT, OR I'LL…
## PANEL 2
### DIALOGUE
   Bill: OR YOU'LL DO WHAT? YOUR KIND NEVER DOES ANYTHING! IT'S ALL DONE FOR YOU!
   Curt: UGGHH…
## PANEL 3
### CAPTION
   THEN, WITHOUT ANOTHER WORD, BILL WAS GONE! I DIDN'T KNOW WHAT TO THINK… WHAT TO DO. BY THE TIME I HAD COME TO MY SENSES, IT WAS TOO LATE!
### DIALOGUE
   Gloria: I'M SORRY, MY DEAR! THE YOUNG MAN BURST IN, PACKED AND LEFT! HE DIDN'T EVEN STOP TO COLLECT HIS PAY!
   Cynthia: GONE! BUT WHERE..
## PANEL 4
### CAPTION
   THERE WAS ONLY ONE CHANCE… THE RAILROAD STATION.
### DIALOGUE
   Cynthia: HE'S GOT TO BE HERE! HE'S GOT TO BE… BILL, BILL! IT'S YOU…
   Bill: WHA… CYNTHY!
## PANEL 5
### CAPTION
   I WANTED TO TAKE HIM IN MY ARMS… TO TELL HIM THAT ALL THE THINGS HE HAD THOUGHT ABOUT ME WEREN'T TRUE… BUT I WAS AFRAID. AFRAID HE DIDN'T WANT ME!
### DIALOGUE
   Cynthia: WHERE ARE YOU GOING?
   Bill: I DON'T KNOW! I… AHH, WHO AM I KIDDING, ANYWAY?.. I'M GOING HOME… BACK TO INDIANA!
## PANEL 6
### CAPTION
   SUDDENLY, I WAS UNREASONABLY ANGRY!
### DIALOGUE
   Cynthia: THEN YOU'RE REALLY GIVING UP! AS LONG AS YOU WERE IN CONNECTICUT YOU COULD ALWAYS GO DOWN AND TRY AGAIN, BUT YOU'RE GOING ALL THE WAY BACK THIS TIME! YOU'RE QUITTING!
   Bill: I'M GETTING SICK OF THE WAY YOU ACCUSE ME! WAIT TILL YOU'VE FOUGHT FOR SOME-THING AND HAVEN'T GOTTEN IT! YOU DON'T KNOW WHAT YOU'RE TALKING ABOUT!
## PANEL 7
### DIALOGUE
   Cynthia: OH, BILL… I'M SORRY! YOU'RE RIGHT… I SHOULDN'T HAVE SAID THAT!
   Bill: OH, CYNTHY CYNTHY! I DON'T KNOW WHAT GOT INTO ME! I DIDN'T MEAN TO JUMP AT YOU THAT WAY! I.. I JUST COULDN'T FACE NEW YORK AGAIN!

# PAGE 7 - 4 PANELS
## PANEL 1
### CAPTION
   AT THAT MOMENT, I KNEW WHAT I WANTED TO DOI
## PANEL 2
### DIALOGUE
   Cynthia: I… I THINK I KNOW HOW YOU FEEL, BILL! COULD YOU… COULD YOU FACE IT WITH SOMEBODY ELSE? COULD YOU, BILL?
   Bill: YOU MEAN… YOU? YOU'D MARRY ME?
## PANEL 3
### DIALOGUE
   Cynthia: OH, YES, DARLING! YES! I'LL MARRY YOU ANYTIME… ANYWHERE! I LOVE YOU SO!
   Bill: OH, DARLING… I LOVE YOU, TOO! WITH YOU TO GIVE ME COURAGE, I KNOW I CAN MAKE GOOD!
### CAPTION
   TELEPHONE
## PANEL 4
### CAPTION
   I COULD TASTE THE SALT OF MY TEARS AS I KISSED HIM, BUT THEY WERE TEARS OF HAPPINESS!
   WE WOULD FACE IT TOGETHER COULD LICK THE WORLD IF WE HAD TO! AND I KNEW WE
   I HAD BEEN LUCKIER THAN MOST. BE-CAUSE HAD ONCE. HAD EVERYTHING, AND I HAD LEARNED THAT IT WAS NOTHING WITHOUT LOVE KNOW IT SOUNDS CORNY, BUT MOST CORNY THINGS ARE TRUE WITHOUT NOTHING LOVE, ELSE MATTERS!
   THE END
### DIALOGUE
   Cynthia: I'LL BE SO GOOD TO YOU, BILL! I'LL MAKE YOU SO HAPPY!
   Bill: LET'S HURRY HONEY! CITY HALL CLOSES AT FOUR O CLOCK!
   