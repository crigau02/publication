# PAGE 1 - 3 PANELS
## PANEL 1
In this comic book panel, we have four characters: Marco, Baby, Leonardo, and an unnamed character in the background. Marco is on the left, playing a guitar while wearing sunglasses and a red shirt. Baby is seated next to him, dressed in a blue dress and looking surprised. On the right, Leonardo is running away from Marco and Baby, with a panicked expression on his face. In the background, there's a person in a purple shirt looking at Leonardo, seemingly observing his actions. The comic panel also includes a speech bubble from Marco, which reads, "I am not asking for the moon?," and another speech bubble from Baby, saying, "Look! There's Leonardo!" The scene takes place on a street with buildings in the background, including a shop with a sign that says "Cafe."
### CAPTION
   WIPO WORLD
   INTELLECTUAL PROPERTY ORGANIZATION
   indecopi
   CAFE
   LEONARDO HAS DONE NOTHING WRONG, BUT WHAT IS BOTHERING HIM? WHO IS HE RUNNING FROM?
### SOUND
   WATCHES
   PATENTS
### DIALOGUE
   Marco: I AM NOT ASKING FOR THE MOON?
   Babi: LOOK! THERE'S LEONARDO!
## PANEL 2
In this comic book panel, Leonardo is shown carrying a large cardboard box. The box is depicted as a plain, light orange color suggesting it is a standard cardboard package or shipping box. Leonardo appears to be exerting effort to carry or lift the sizable box.
## PANEL 3
The comic panel shows a indoor setting, with one character standing and gesturing, while another character stands nearby expressing disagreement. The text in the speech bubbles indicates a conversation, with one character stating "THAT GUY IS CRAZY..." and the other replying "IT'S NOT TRUE!". The third character, named Leonardo, remains silent during this exchange. The characters' postures and dialogue suggest a disagreement or debate unfolding between them in this domestic indoor scene.
### DIALOGUE
   Marco: THAT GUY IS CRAZY…
   Babi: IT'S NOT TRUE!
   Leonardo:
## PANEL 4
In a cartoon-style comic book panel, a woman with a pearl necklace, wearing a pink blouse, and red skirt stands in the foreground. She is addressing a man in the background, who appears to be holding a stack of papers or books. The woman's dialogue, as indicated in the speech bubble, reads, "DO YOU WANT SOMETHING?" in white letters within a black rectangle. The man, identified as "Leonardo," responds with a nervous intake of breath, "( GULP ) ER, NO…" also in white letters within a black rectangle. The scene is set against a plain yellow background, with the continuation of the story or message hinted at by the phrase "BUT READ ON!" written in a white rectangle at the bottom of the panel.
### CAPTION
BUT READ ON!
### DIALOGUE
   c5: DO YOU WANT SOMETHING?
   Leonardo: ( GULP ) ER, NO…

# PAGE 2 - 8 PANELS
## PANEL 1
The comic book panel shows a close-up view of a cartoon character's face, drawn in a simple, colorful style reminiscent of early 2000s web comics. The character has yellow hair, large round eyes, and a prominent nose. The character's expression appears thoughtful or pensive. In the reflections of their glasses, we see a small male cartoon figure with blond hair who seems to be the subject of the main character's statement. Above the image is a speech bubble with the text "HE IS QUITE A CHARACTER..." This line of dialogue is attributed to a character named Marco, based on the script excerpt provided. The overall tone of the panel seems lighthearted, likely meant to humorously highlight some personality quirk or behavior of the smaller character reflected in the glasses, who Marco is referring to as "quite a character". The simple artwork style and zoomed-in framing on the speaking character's face helps emphasize their facial expression reacting to this other individual.
### DIALOGUE
   Marco: HE IS QUITE A CHARACTER…
   Leonardo:
## PANEL 2
In this comic book panel, we see a character named Babi speaking the line "HE'S OK. ONCE HE HELPED ME FIX UP MY BIKE." Babi is depicted twice in this panel. On the left side of the panel, we see Babi in the background, standing and looking on as another character, Leonardo, is crouched down working on a pink bicycle. There are labels indicating the names of these two characters. Babi in the background is wearing a black shirt with a red scarf or collar and grey pants, while Leonardo is wearing a yellow shirt and blue jeans. On the right side of the panel, a close-up of Babi's face is shown as she speaks the dialogue. She appears to be looking to the side and has a neutral expression. Her hair is styled in tight curls, and she is wearing large red earrings. The speech bubble with her dialogue emerges from this close-up image of Babi. The background is a simple flat color, and the overall style of the artwork is bold and cartoonish with thick outlines and vibrant colors.
### DIALOGUE
   Babi: HE'S OK. ONCE HE HELPED ME FIX UP MY BIKE.
   Leonardo:
## PANEL 3
In a vibrant comic book panel, we find Leonardo, a character with a mop of curly blonde hair, engrossed in a moment of invention. He crouches beside a bicycle, his eyes keen with concentration as he affixes a bright orange bulb horn to the bike's wheel. He wears a striking yellow shirt that contrasts with the deep purple of the bike frame.Leonardo seems eager to share his discovery, as he declares with a hint of pride, "WITH THIS BULB HORN YOU CAN MAKE A NOISE JUST LIKE A MOTORBIKE…" The words float above him in a bold, white speech bubble, edged with black to stand out against the pastel yellow backdrop. The scene captures the essence of imagination and play—a moment where a simple object is transformed into a source of joy and endless possibility in the hands of a creative soul. Leonardo's inventive spirit is palpable, promising that, with a squeeze of that bulb, the quiet street will soon echo with the sounds of a roaring motorbike, all thanks to his clever contraption.
### DIALOGUE
   Leonardo: WITH THIS BULB HORN YOU CAN MAKE A NOISE JUST LIKE A MOTORBIKE…
## PANEL 4
In this comic book panel, we see a young girl named Babi energetically riding her pink bicycle. She is wearing a red shirt and a white headband, indicating her active nature. Babi is smiling, suggesting she is enjoying the ride. Her legs are pedaling rapidly, and the sound effect "RRRRR" is depicted, indicating the speed at which she is cycling. The motion of the wheels and the sound effect together create a sense of movement and excitement.
### SOUND
   RRRR
### DIALOGUE
   Babi:
## PANEL 5
In this panel, we see two characters walking side by side, with the name tags "Marco" and "Babi" above them, respectively. The background is a vibrant pink, and the characters are depicted in a yellow silhouette. The speech bubble comes from Babi, emphasizing that "other guys make fun of him." This suggests that the scene is addressing a social issue, possibly bullying or teasing, that one character is facing due to the behavior of others.
### DIALOGUE
   Babi: BUT OTHER GUYS MAKE FUN OF HIM…
## PANEL 6
In this vibrant comic book panel, we see a scene of five individuals, each with unique expressions and postures, against a purple background. The mood is animated and intense. At the top of the panel, in bold, jagged lettering, the sound effects scream out the words "CRAZY GUY! NUTCASE!" setting an exclamatory tone. Focusing on the characters, to the left is a person with their head turned towards the viewer, mouth open as if shouting, only the side of their face and ear visible. Next to this person is Babi, who is defending someone's character. Babi is drawn with a serious expression, furrowed brow, and lips firmly stating, "HE MAY BE DIFFERENT, BUT HE IS CERTAINLY NO NUTCASE." In the center, we have a smiling figure, perhaps the subject of the surrounding controversy, unaware or unfazed by the comments. This person is labeled Leonardo, but their dialogue is left unknown, creating an air of mystery about their response or thoughts. On the right, there are two more characters engaged in the scene. One appears to be a young person, mouth open as if echoing the sound effects or contributing to the commotion. The other is facing towards Leonardo, hand raised with a pointed finger, possibly accusing or singling out Leonardo as the "CRAZY GUY! NUTCASE!". The remaining dialogue bubbles are intentionally left blank, inviting us to imagine what might be said next in this heated exchange.
### SOUND
   CRAZY GUY! NUTCASE!
### DIALOGUE
   Babi: HE MAY BE DIFFERENT, BUT HE IS CERTAINLY NO NUTCASE.
## PANEL 7
Ping Song, a young man with short black hair and glasses, is talking to his two friends, Marco and Babi. Marco has short blond hair and a yellow shirt, and Babi has long black hair with a red headband and a pink shirt. Ping Song says, "No, I'm sure he isn't crazy; he could even be smarter than us: he is an inventor, after all."
### DIALOGUE
   Ping Song: NO, I'M SURE HE ISN'T CRAZY; HE COULD EVEN BE SMARTER THAN US: HE IS AN INVENTOR, AFTER ALL.
## PANEL 8
In a vividly colored comic book panel, we see two characters engaging in a lively conversation. One character, with a bright yellow head and a red shirt, exclaims, "An inventor? I don't believe you!" His name, Marco, is displayed in a white rectangle. Opposite him, another character, sporting a black hairstyle and a blue shirt, confidently asks, "Why not?" His name, Babi, is also shown in a white rectangle. The contrasting colors of their clothes and hair add to the dynamic energy of their exchange, as they stand against a plain blue backdrop. The speech bubbles capture their dialogue, highlighting the playful and inquisitive spirit of their interaction.
### DIALOGUE
   Marco: AN INVENTOR? I DON'T BELIEVE YOU!
   Babi: WHY NOT?

# PAGE 3 - 5 PANELS
## PANEL 1
### DIALOGUE
   Marco: BECAUSE FOR THAT YOU HAVE TO BE AN INTELLECTUAL.
   Marco: FOR INSTANCE THE MAN WHO INVENTED ELECTRICITY…
   Marco: ENGINES…
   Marco: COMPUTERS…
## PANEL 2
### DIALOGUE
   Ping Song: INVENTIONS COME IN ALL SHAPES AND SIZES, FROM THE SIMPLEST TO THE MOST COMPLEX.
## PANEL 3
### DIALOGUE
   Ping Song: INVENTORS ARE PEOPLE WHO REFLECT ON VARIOUS PROBLEMS AND WORK OUT TECHNICAL SOLUTIONS…
   Babi: IF THE SOLUTION IS A NEW ONE, IT IS AN INVENTION.
## PANEL 4
### CAPTION
   BANK
   CP-NNER-A
### DIALOGUE
   Marco: WOW! JUST LOOK ANYWHERE, EVERYTHING HAS BEEN INVENTED!
   Ping Song: THAT'S RIGHT, MANKIND OWES A LOT TO INVENTORS…
## PANEL 5
### CAPTION
   ARV
### DIALOGUE
   Babi: HEY, LOOK! THOSE PEOPLE ARE CHASING LEONARDO!

# PAGE 4 - 7 PANELS
## PANEL 1
### DIALOGUE
   Marco: I KNOW THOSE GUYS! THEY'RE THIEVES!
## PANEL 2
### DIALOGUE
   Marco: WE HAVE TO HELP THE NUTCASE…
   Babi: HE IS NOT A NUTCASE!
## PANEL 3
### CAPTION
   FLOWE
### DIALOGUE
   Babi: WHAT CAN HE BE CARRYING IN THAT BOX?
   Ping Song: I SUPPOSE IT'S AN INVENTION.
## PANEL 4
### DIALOGUE
   Ping Song: THERE'S A FRIEND OF OURS WHO WILL KNOW HOW TO PROTECT IT BETTER…
   Babi: CHULI!
## PANEL 5
### DIALOGUE
   Marco: I'M GOING TO CALL HER RIGHT NOW!
## PANEL 6
### SOUND
   DRING
## PANEL 7
### SOUND
   ..?
### DIALOGUE
   Marco: **by phone** CHULI, IT'S MARCO, LISTEN: I AM WITH BABI AND PING SONG, AND WE ARE FOLLOWING LEONARDO THE INVENTOR, BECAUSE HE'S BEING CHASED BY SOME THIEVES WHO SEEM TO BE AFTER HIS INVENTION, AND…
   Chuli: ...?

# PAGE 5 - 7 PANELS
## PANEL 1
### DIALOGUE
   Chuli: WHERE ARE THEY?
   Marco: **by phone** IN CENTRAL AVENUE, CLOSE TO SUNSHINE SQUARE…
## PANEL 2
### SOUND
   RRRROUK
### DIALOGUE
   Chuli: **thinking** I'M QUITE NEAR THERE!
## PANEL 3
### DIALOGUE
   c0:
   c7:
   Leonardo:
## PANEL 4
### DIALOGUE
   c0:
   c7:
   Leonardo:
## PANEL 5
### DIALOGUE
   c0:
   c7:
   Leonardo:
## PANEL 6
### SOUND
   RRROUMRRR
### DIALOGUE
   Chuli: LEONARDO! GET ON, QUICKLY!
## PANEL 7
### DIALOGUE
   c0: @ * # ¢!
   c7: GRR! GRRRR!

# PAGE 6 - 6 PANELS
## PANEL 1
### DIALOGUE
   Marco: HA HA HA! WHAT A GREAT INVENTION THE SCOOTER IS!
## PANEL 2
### DIALOGUE
   c0: SO WHAT ARE YOU LAUGHING AT, FATSO?
   c7: GRR! GRRRR!
   c0: **thinking** GULP!
## PANEL 3
### DIALOGUE
   Marco: WHO, ME? HEH, HEH… I WAS JUST TELLING MY FRIEND A JOKE… ABOUT A PARROT THAT…
   c0: YAH! GO SHUT YOUR FACE!
   c7: GRR! GRRRR!
## PANEL 4
### DIALOGUE
   Babi: I KNOW WHERE CHULI AND LEONARDO HAVE GONE… LET'S GET A TAXI…
## PANEL 5
### CAPTION
   LATER…
### DIALOGUE
   Ping Song:
   Babi:
   Marco:
## PANEL 6
### CAPTION
   TAXI
   LOOK OUT! WE KNOW THIS MAN!
### DIALOGUE
   Babi: LET'S GO TO THE PATENT OFFICE.

# PAGE 7 - 7 PANELS

## PANEL 1
### DIALOGUE
   Marco: WHAT IS A PATENT?
   Babi: IT'S A SORT OF CERTIFICATE THAT THE STATE GIVES TO THE OWNER OF AN INVENTION.
## PANEL 2
### DIALOGUE
   Babi: THE PATENT GIVES HIM THE EXCLUSIVE RIGHT TO EXPLOIT HIS INVENTION…
## PANEL 3
### CAPTION
   THE TERM OR LIFE OF A PATENT DEPENDS ON THE COUNTRY. IT IS USUALLY 20 YEARS.
## PANEL 4
### CAPTION
   PATENT OFFICE
### DIALOGUE
   Chuli: HERE WE ARE!
   Leonardo: AND WHAT IF THE INVENTION IS NOT COMPLETELY NEW, BUT IMPROVES PART OF ANOTHER ONE WHICH IS ALREADY KNOWN?
## PANEL 5
### DIALOGUE
   Chuli: THAT IS CALLED A UTILITY MODEL AND IT QUALIFIES FOR PROTECTION AS WELL.
   Leonardo: THERE IS A PROTECTION, EVEN FOR THAT?
## PANEL 6
### DIALOGUE
   Chuli: WELL, YES, PROVIDED THAT THE CHANGE GIVES THE OBJECT A NEW USEFULNESS, ADVANTAGE OR EFFECT.
## PANEL 7
### CAPTION
   PATENT OFFICE
### DIALOGUE
   Chuli: IN FACT THERE IS STILL ANOTHER KIND OF PROTECTION FOR THE INDUSTRIAL DESIGN EMBODIED IN THE OBJECT.
   Leonardo: SO THERE THE DESIGN IS THE MAIN THING!

# PAGE 8 - 7 PANELS
## PANEL 1
### DIALOGUE
   Marco: BUT WHO HAS THE STRONGER RIGHT… THE ONE WHO MADE THE INVENTION FIRST OR THE FIRST ONE TO APPLY FOR THE PATENT?
## PANEL 2
### DIALOGUE
   Ping Song: IN MOST COUNTRIES THE ONE WHO APPLIES FOR THE PATENT. SO DO YOU SEE NOW HOW IMPOR-TANT IT IS TO PATENT INVENTIONS WITHOUT DELAY?
## PANEL 3
### DIALOGUE
   Marco: DO I JUST! FILING A PATENT APPLI-CATION IS IMPORTANT WITH A CAPITAL “ I ”.
   c0: **thinking** SO I'VE STILL GOT TIME!
## PANEL 4
### DIALOGUE
   Leonardo: AND WHAT DOES AN INVENTION HAVE TO OFFER BEFORE IT QUALIFIES FOR A PATENT?
## PANEL 5
### DIALOGUE
   Chuli: GENERALLY THERE ARE THREE REQUIREMENTS: NOVELTY, INVENTIVE STEP OR NON-OBVIOUSNESS AND INDUSTRIAL APPLICABILITY.
   Leonardo: …?
## PANEL 6
### DIALOGUE
   Leonardo: WHOA, NOT SO FAST, PLEASE.
## PANEL 7
### CAPTION
   1 NOVELTY MEANS THAT THE INVENTION MUST NOT BE KNOWN.
   BATA FRI 02 15: 2100

# PAGE 9 - 5 PANELS
## PANEL 1
### CAPTION
   2 INVENTIVE STEP MEANS THAT IT MUST NOT BE OBVIOUS TO PEOPLE WHO KNOW THE TECHNICAL FIELD CONCERNED.   
## PANEL 2
### CAPTION
   3 INDUSTRIAL APPLICABILITY MEANS THE INVENTION MUST BE CAPA-BLE OF BEING PRODUCED OR USED ON AN INDUSTRIAL SCALE.
## PANEL 3
### DIALOGUE
   Leonardo: CHULI… I THINK MY INVENTION MEETS THOSE REQUIREMENTS…
## PANEL 4
### CAPTION
   TAXI
   AD-667
### DIALOGUE
   Marco: AND WHAT COULD LEONARDO'S INVENTION BE?
   Babi: I DON'T KNOW.
## PANEL 5
### DIALOGUE
   Babi: IT HAS TO BE SOMETHING VERY USEFUL TO MANKIND.
   Marco: IT HAS TO BE SOMETHING THAT'S WORTH A LOT OF MONEY!

# PAGE 10 - 6 PANELS
## PANEL 1
### DIALOGUE
   Marco: BY THE WAY, WHO DO THE RIGHTS BELONG TO, THE INVENTOR OR THE ONE WHO OWNS THE PATENT?
## PANEL 2
### DIALOGUE
   Ping Song: THE ONE WHO OWNS THE PATENT, AS THE INVENTOR MAY HAVE TRANSFERRED HIS RIGHTS.
## PANEL 3
### DIALOGUE
   Marco: SO THE RIGHTS CAN BE TRANSFERRED?
## PANEL 4
### DIALOGUE
   Ping Song: OH YES, OF COURSE. THE PATENT IS A TITLE THAT CAN BE EITHER SOLD OR ASSIGNED…
## PANEL 5
### DIALOGUE
   Leonardo: I'LL SHOW YOU MY INVENTION.
   Chuli:
## PANEL 6
### CAPTION
   ①De my when has time ② Ammet love when ③ bunteren a your enthu Ⓒ hen ad gunt
### DIALOGUE
   Chuli: WHAT IS IT? A LAMP?
   Leonardo: SORT OF. ACTUALLY IT'S A TRAP FOR CATCHING INSECTS.

# PAGE 11 - 6 PANELS
## PANEL 1
### DIALOGUE
   Chuli: YOU KNOW, LEONARDO, YOUR INVENTION WILL BE VERY USEFUL IN AGRICULTURE!
## PANEL 2
### DIALOGUE
   Leonardo: I WOULD LIKE THIS TO BE USEFUL TO PEOPLE…
## PANEL 3
### CAPTION
   PATENT OFFICE
### DIALOGUE
   c0: HERE WE ARE.
## PANEL 4
### DIALOGUE
   c0: **thinking** SO NOW I JUST DASH IN AND GRAB THE INVENTION BEFORE IT IS PATENTED…
   Ping Song: HERE, THANKS.
## PANEL 5
### DIALOGUE
   c0: SAY, ER… LOOK HERE, I'D LIKE TO GO ALONG WITH YOU ON THIS PATENTING JOB…
   Marco: ...?
   Ping Song: ...?
   Babi: ...?
## PANEL 6
### CAPTION
   TAXI
### DIALOGUE
   Marco: WAIT A MINUTE, THIS IS THE ONE WHO WAS FOLLOWING TO…
   Ping Song: **shouting** POLICE!

# PAGE 12 - 5 PANELS
## PANEL 1
### SOUND
   RRRRRRRRRRMMMMR
### DIALOGUE
   Chuli: HEY, YOU GUYS! LEONARDO HAS JUST FILED A PATENT APPLICATION FOR HIS INVENTION!
   Marco: WELL DONE!
   Ping Song: WELL DONE!
   Babi: WELL DONE!
## PANEL 2
### DIALOGUE
   Marco: TELL ME, WHAT DID YOU WRITE DOWN WHEN YOU STOPPED BY THAT GATE?
   Leonardo: OH… THAT…
## PANEL 3
### DIALOGUE
   Leonardo: OH, NOTHING REALLY… I JUST THOUGHT THE PADLOCK COULD BE COVERED UP…
## PANEL 4
### DIALOGUE
   Leonardo: IT'S NOT A TEMPTATION FOR THIEVES, AND IT LOOKS NICER…
## PANEL 5
### CAPTION
   PA OFFICE
   END
### DIALOGUE
   Marco: AND YOU CALL THAT NOTHING? YOU CAN PATENT THAT TOO!
   Leonardo: THAT TOO? BUT I'VE GOT NO END OF IDEAS LIKE THAT!
   Chuli: PATENTS NOT ONLY PROTECT YOUR INVENTIONS: THEY CAN EARN YOU PROFITS SO, YOU CREATIVE PEOPLE, START PATENTING!
