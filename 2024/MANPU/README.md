Toward accessible comics for blind and low vision readers
===============================================
   
Abstract
------------------------------------------------
This work explores how to fine-tune large language models using prompt engineering techniques with contextual information for generating an accurate text description of the full story, ready to be forwarded to off-the-shelve speech synthesis tools. We propose to use existing computer vision and optical character recognition techniques to build a grounded context from the comic strip image content, such as panels, characters, text, reading order and the association of bubbles and characters. Then we infer character identification and generate comic book script with context-aware panel description including character’s appearance, posture, mood, dialogues etc. We believe that such enriched content description can be easily used to produce audiobook and eBook with various voices for characters, captions and playing sound effects.

Citation BiBteX
-------------------------------------------------
Published in the proceedings of the 6th International Workshop on [coMics ANalysis, Processing and Understanding](https://manpu2024.imlab.jp "MANPU"), organized in conjunction with the [18th International Conference on Document Analysis and Recognition](https://icdar2024.net "ICDAR") as "ICDAR pre-conference volume" edited by Springer in the [Lecture Notes in Computer Science](https://www.springer.com/gp/computer-science/lncs/ "LNCS") series.

ArXiv [PDF](https://arxiv.org/abs/2407.08248)
HAL [PDF](https://hal.science/hal-04610678)

<pre><code>
@misc{rigaud2024accessiblecomicsblindlow,
      title={Toward accessible comics for blind and low vision readers}, 
      author={Christophe Rigaud and Jean-Christophe Burie and Samuel Petit},
      year={2024},
      eprint={2407.08248},
      archivePrefix={arXiv},
      primaryClass={cs.AI},
      url={https://arxiv.org/abs/2407.08248}, 
}
</code></pre>
